output "id" {
  description = "ID of the created API gateway"
  value       = aws_apigatewayv2_api.app.id
}