data "aws_ecr_repository" "repo" {
  name = var.function_name
}


data "aws_ecr_image" "lambda_image" {
  repository_name = data.aws_ecr_repository.repo.name
  image_tag       = var.image_tag
}


data "aws_iam_policy_document" "lambda_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}


resource "aws_iam_role" "lambda" {
  name               = "${var.function_name}-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_policy.json
}


resource "aws_iam_role_policy_attachment" "lambda_cloudwatd_policy_attachment" {
  role       = aws_iam_role.lambda.id
  policy_arn = var.lambda_policy_arn
}


resource "aws_lambda_function" "minecraft-status" {
  function_name = "${var.function_name}-lambda"
  role          = aws_iam_role.lambda.arn
  timeout       = 300
  image_uri     = "${data.aws_ecr_repository.repo.repository_url}@${data.aws_ecr_image.lambda_image.id}"
  package_type  = "Image"

  environment {
    variables = {
      MC_SERVER_ADDR = var.mc_server_hostname
      MC_SERVER_PORT = var.mc_server_port
    }
  }
}

module "cloudflare_cert" {
  source = "../cloudflare_verified_cert/"

  hostname    = var.function_name
  domain_name = var.domain_name
}

module "api_gateway" {
  source      = "../api_gateway/"
  hostname    = var.function_name
  lambda_name = aws_lambda_function.minecraft-status.function_name
  lambda_arn  = aws_lambda_function.minecraft-status.arn
  zone_name   = var.domain_name
  cert_arn    = module.cloudflare_cert.arn
}

module "static-frontend" {
  source = "../static-webpage"

  domain_name = var.domain_name
  app_name    = var.function_name
}

resource "aws_apigatewayv2_integration" "example" {
  api_id           = module.api_gateway.id
  integration_type = "HTTP_PROXY"

  integration_method = "ANY"
  integration_uri    = "http://${module.static-frontend.website_url}/"
}

resource "aws_apigatewayv2_route" "example" {
  api_id    = module.api_gateway.id
  route_key = "$default"

  target = "integrations/${aws_apigatewayv2_integration.example.id}"
}