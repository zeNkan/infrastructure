output "website_url" {
  description = "URL to website"
  value       = aws_s3_bucket.minecraft-status.website_endpoint
}