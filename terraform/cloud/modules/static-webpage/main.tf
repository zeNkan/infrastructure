locals {
  fqdn = "${var.app_name}.${var.domain_name}"
}

resource "aws_s3_bucket" "minecraft-status" {
  bucket = local.fqdn

  website {
    index_document = "index.html"
  }
}

resource "aws_s3_bucket_cors_configuration" "static-webpage" {
  bucket = aws_s3_bucket.minecraft-status.id

  cors_rule {
    allowed_methods = ["GET"]
    allowed_origins = ["https://${local.fqdn}"]
  }
}

resource "aws_s3_bucket_acl" "static-webpage" {
  bucket = aws_s3_bucket.minecraft-status.id
  acl    = "public-read"
}