variable "domain_name" {
  description = "Name of the page"
  type        = string
  default     = "default"
}

variable "app_name" {
  description = "Name of the app"
  type        = string
  default     = "default"
}